package moe.nnsnodnb.edittext

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle


class MessageDialog : DialogFragment() {

    private var title: String? = null
    private var message: String? = null
    private var listener: OnOkButtonClickedListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle): Dialog {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            title = arguments.getString(TITLE)
            message = arguments.getString(MESSAGE)
        }

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title).setMessage(message).setPositiveButton("OK") {
            dialog, id -> listener!!.onOkButtonClicked()
        }
        return builder.create()
    }

    override fun onAttach(context: Context) {
        //リスナーのセット
        super.onAttach(context)
        try {
            listener = activity as OnOkButtonClickedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement OnOkButtonListener")
        }

    }

    override fun onDetach() {
        //リスナーの削除
        super.onDetach()
        listener = null
    }

    interface OnOkButtonClickedListener {
        fun onOkButtonClicked()
    }

    companion object {

        private val TITLE = "title"
        private val MESSAGE = "message"
    }

}