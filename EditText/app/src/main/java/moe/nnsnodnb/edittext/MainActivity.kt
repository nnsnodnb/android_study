package moe.nnsnodnb.edittext

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button) as Button
        button.setOnClickListener {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
            val editText = findViewById(R.id.editText) as EditText
            if (editText.text.length == 0) {
                val dialog = NoDataDialogFragment()
                dialog.show(supportFragmentManager, "dialog")
            }
        }
    }

    class NoDataDialogFragment : DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("文字が入力されていません")
            builder.setMessage("")
            builder.setPositiveButton("OK") { dialog, which ->
                println("OK")
            }
            return builder.create()
        }
    }
}
