package moe.nnsnodnb.dialogfragment

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.widget.Button
import android.widget.Toast


class MainActivity : FragmentActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button: Button = findViewById(R.id.button) as Button
        button.setOnClickListener {
            val dialog = MyDialogFragment()
            dialog.show(supportFragmentManager, "dialog")
        }
    }

    fun doPositiveClick() {
        Toast.makeText(this, "OKボタンがクリックされました", Toast.LENGTH_SHORT).show()
    }

    fun doNegativeClick() {
        Toast.makeText(this, "Cancelボタンがクリックされました", Toast.LENGTH_SHORT).show()
    }

    class MyDialogFragment : DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("タイトル")
            builder.setMessage("メッセージ")
            builder.setPositiveButton("OK") { dialog, which ->
                val activity = activity as MainActivity
                activity.doPositiveClick()
                println("OK")
            }
            builder.setNegativeButton("Cancel") { dialog, which ->
                val activity = activity as MainActivity
                activity.doNegativeClick()
                println("Cancel")
            }
            return builder.create()
        }
    }
}