package moe.nnsnodnb.button

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import java.util.logging.Logger

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pushButton: Button = findViewById(R.id.pushButton) as Button
        val helloButton: Button = findViewById(R.id.helloButton) as Button
        pushButton.setOnClickListener {
            println("push")
        }
        helloButton.setOnClickListener {
            println("hello")
        }
    }
}