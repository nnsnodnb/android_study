package moe.nnsnodnb.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textViewLarge = findViewById(R.id.textViewLarge) as TextView
        val textViewMedium = findViewById(R.id.textViewMedium) as TextView
        textViewLarge.text = "Hello World!"
        textViewMedium.text = "Kotlin"
    }
}