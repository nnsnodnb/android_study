package moe.nnsnodnb.webview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val webView = findViewById(R.id.webView) as WebView
        webView.settings.javaScriptEnabled = true

        webView.setWebViewClient(WebViewClient())
        webView.loadUrl("https://github.com/")

        val backButton = findViewById(R.id.backButton) as Button
        backButton.setOnClickListener {
            if (webView.canGoBack()) {
                webView.goBack()
            }
        }
        val forwardButton = findViewById(R.id.forwardButton) as Button
        forwardButton.setOnClickListener {
            if (webView.canGoForward()) {
                webView.goForward()
            }
        }
        val reloadButton = findViewById(R.id.reloadButton) as Button
        reloadButton.setOnClickListener {
            webView.reload()
        }
    }
}